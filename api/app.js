'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

//Cargar rutas de la aplicación
var user_routes = require('./routes/user');
var follow_routes = require('./routes/follow');

//Middlewares

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// CORS Para permitir peticiones entre front y backend

//Rutas de la aplicación
app.use('/api', user_routes);
app.use('/api', follow_routes);
//Exportación
module.exports = app;
'use strict'

var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');

var User = require('../models/user');
var Follow = require('../models/follow')
var jwt = require('../services/jwt');
//-------- METODOS DE PRUEBA --------//
function home(req, res){
    res.status(200).send({
        message: 'Hola desde el controlador de usuarios'
    });
};

function pruebas(req, res){
    console.log(req.body);
    res.status(200).send({
        message:'Acción de pruebas en el controlador de usuarios'
    });
};
//-------- REGISTRO DE USUARIOS --------//
function saveUser(req, res){
    var params = req.body;
    var user = new User();

    if(params.name && params.surname &&
        params.nick && params.email && params.password){

            user.name = params.name;
            user.surname = params.surname;
            user.nick = params.nick;
            user.email = params.email;
            user.role = 'ROLE_USER';
            user.image = null;

            //Control para comprobar usuarios duplicados
            User.find({ $or: [
                {email: user.email.toLowerCase()},
                {nick: user.nick.toLowerCase()}
            ]}).exec((err, users) => {
                if(err) return res.status(500).send({message:'Error en la petición de registrar usuario'});
                if(users && users.length >=1){
                    return res.status(200).send({message:'Este usuario ya existe'});
                }else{
                    //Cifrado de contraseñas
                    bcrypt.hash(params.password, null, null, (err, hash) => {
                    user.password = hash;

                    user.save((err, userStored) => {
                    if(err) return res.status(500).send({message:'Error al guardar el usuario'});

                    if(userStored){
                        res.status(200).send({user: userStored});
                    }else{
                        res.status(400).send({message:'No se ha registrado el usuario'});
                    }
                });
                });
              }
            });
            
    
        }else{
            res.status(200).send({
                message:'Envia todos los campos necesarios'
            });
        }
}

//-------- LOGIN DE USUARIOS --------//
function loginUser(req, res) {
    var params = req.body;

    var email = params.email;
    var password = params.password;

    User.findOne({email: email}, (err, user) =>{
        if(err) return res.status(500).send({message:'Error en la petición'});

        if(user){
            bcrypt.compare(password, user.password, (err, check) => {
                if (check){

                    if(params.gettoken){
                        //Generar y devolver un token
                            return res.status(200).send({
                                token: jwt.createToken(user) 
                            });
                    }else{
                        //Devolver datos de usuario
                        user.password = undefined;
                        return res.status(200).send({user});
                    }

                }else{
                    return res.status(404).send({message:'El usuario no se ha podido identificar'});
                }
            });
        }else{
            return res.status(404).send({message:'El usuario no se ha podido identificar D8'});
        }
    });
}

//-------- CONSEGUIR DATOS DE USUARIO ESPECIFICO --------//
function getUser(req, res){
    var userId = req.params.id;

    User.findById(userId, (err, user) => {
        if(err) return res.status(500)({message:'Error en la petición'});

        if(!user) return res.status(404)({message:'El usuario no existe'});

        followThisUser(req.user.sub, userId).then((value) => {
            return res.status(200).send({
                user,
                following: value.following,
                followed: value.followed
            });
        });
    });
}

//-------- FUNCION ASINCRONA PARA COMPROBAR SI UN USUARIO ME SIGUE --------//
async function followThisUser(identity_user_id, user_id){
    var following = await Follow.findOne({"user":identity_user_id, "followed":user_id}).exec((err, follow) => {
        if(err) return handleError(err);
        return follow;
    });

    var followed = await Follow.findOne({"user":user_id, "followed":identity_user_id}).exec((err, follow) => {
        if(err) return handleError(err);
        return follow;
    });

    return {
        followed: followed,
        following: following
    }
}

//-------- DEVOLVER UN LISTADO DE USUARIOS PAGINADO --------//
function getUsers(req, res){
    var identity_user_id = req.user.sub;

    var page = 1;
    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 5;

    User.find().sort('_id').paginate(page, itemsPerPage, (err, users, total) => {
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!users) return res.status(404).send({message: 'No hay usuarios disponibles'});

        followUserIds(identity_user_id).then((value)=>{


            return res.status(200).send({
                users,
                users_following: value.following,
                users_follow_me: value.followed,
                total,
                pages: Math.ceil(total/itemsPerPage)

            });
        });
    })
}

async function followUserIds(user_id){
    var following = await Follow.find({"user":user_id}).select({'_id':0, '_v':0,'user':0}).exec((follows) =>{
        return follows;
    });

    var followed = await Follow.find({"followed":user_id}).select({'_id':0, '_v':0,'user':0}).exec((follows) =>{
        return follows;
    });

    //Procesar following por ids
    var following_clean = [];

    following_clean.forEach((follow)=>{
        following_clean.push(follow.followed);
    });

    //Procesar followed ids
    var followed_clean = [];

    followed_clean.forEach((follow)=>{
        followed_clean.push(follow.user);
    });

    return {
        following: following_clean,
        followed: followed_clean
    }
}

function getCounters(req, res){
    var userId = req.user.sub;
    if(req.params.id){
        userId = req.params.id;
    }

    getCountFollow(userId).then((value)=>{
        return res.status(200).send(value);
    });
}

async function getCountFollow(user_id){
    var following = await Follow.count({"user":user_id}).exec((err, count) => {
        if(err) return handle(err);
        return count;
    });
    
    var followed = await Follow.count({"followed":user_id}).exec((err, count) => {
        if(err) return handle(err);
        return count;
    });

    return{
        followed: followed,
        following: following
    }
}

//-------- ACTUALIZAR DATOS DE USUARIO --------//
function updateUser(req, res){
    var userId = req.params.id;
    var update = req.body;

    delete update.password;
    if(userId != req.user.sub){
        return res.status(500).send({message:'No tienes permiso para actualizar datos de usuario'});
    }

    User.findByIdAndUpdate(userId, update, {new:true}, (err, userUpdated) => {
        if(err) return res.status(500).send({message:'Error en la petición'});

        if(!userUpdated) return res.status(400).send({message:'No se pudo actualizar el usuario'});

        return res.status(200).send({user: userUpdated});
    });
}

//-------- SUBIR ARCHIVOS DE IMAGEN/AVATAR DE USUARIO --------//
function uploadImage(req, res){
    var userId = req.params.id;

    if(req.files){
        var file_path = req.files.image.path;
        console.log(file_path);

        var file_split = file_path.split('\\');
        console.log(file_split);

        var file_name = file_split[2];
        console.log(file_name);

        var ext_split = file_name.split('\.');
        console.log(ext_split);

        var file_ext = ext_split[1];
        console.log(file_ext)

        if(userId != req.user.sub){
            return removeFilesOfUploads(res, file_path,'No tienes permiso para actualizar datos de usuario');
        }

        if(file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif'){
            //Actualizar documento de usuario logueado
            User.findByIdAndUpdate(userId,{image: file_name},{new: true},(err, userUpdated) =>{
                if(err) return res.status(500).send({message:'Error en la petición'});

                if(!userUpdated) return res.status(400).send({message:'No se pudo actualizar el usuario'});
        
                return res.status(200).send({user: userUpdated});
            });
        }else{
            return removeFilesOfUploads(res, file_path,'Extensión no valida');
        }
    }else{
        return res.status(200).send({message:'No se han subido imagenes'});
    }
}

//-------- FUNCIÓN AUXILIAR PARA SUBIR IMAGENES --------//
function removeFilesOfUploads(res, file_path, message){
    fs.unlink(file_path, (err)=>{
        return res.status(200).send({message: message});
    });
}
//-------- MOSTRAR IMAGEN DE USUARIO --------//
function getImage(req, res){
    var image_file = req.params.imageFile;
    var path_file = './uploads/users/'+image_file;

    fs.exists(path_file, (exist) => {
        if(exist){
            res.sendFile(path.resolve(path_file));
        }else{
            res.status(200).send({message: 'No existe la imagen'});
        }
    });
}

module.exports = {
    home,
    pruebas,
    saveUser,
    loginUser,
    getUser,
    getUsers,
    getCounters,
    updateUser,
    uploadImage,
    getImage
}
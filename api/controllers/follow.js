'use strict'

var moongosePaginate = require('mongoose-pagination');

var User = require('../models/follow');
var Follow = require('../models/follow');

//-------- GUARDAR SEGUIMIENTO DE UN USUARIO A OTRO --------//
function saveFollow(req, res){
    var params = req.body;
    
    var follow = new Follow();
    follow.user = req.user.sub;
    follow.followed = params.followed;

    follow.save((err, followStored) =>{
        if(err) return res.status(500).send({message:'Erro al guardar el seguimiento'});

        if(!followStored) return res.status(404).send({message:'El seguimiento no se ha guardado'});

        res.status(200).send({follow:followStored});
    })
}

//-------- DEJAR DE SEGUIR A USUARIOS --------//
function deleteFollow(req, res){
    var userId = req.user.sub;
    var followId = req.params.id;

    Follow.find({'user':userId, 'followed':followId}).remove(err => {
        if(err) return res.status(500).send({message:'Error al borrar el seguimiento'});

        return res.status(200).send({message: 'El follow se ha eliminado!'});
    });
}

//-------- LISTADO DE USUARIOS QUE SIGO --------//
function getFollowingUsers(req, res){
    var userId = req.user.sub;

    if(req.params.id && req.params.page){
        userId = req.params.id;
    }

    var page = 1;

    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 4;

    Follow.find({user:userId}).populate({path:'followed'}).paginate(page, itemsPerPage, (err, follows, total) => {
        if(err) return res.status(500).send({message:'Error en servidor'});

        if(!follows) return res.status(404).send({message: 'No estas siguiendo a ningun usuario'});

        return res.status(200).send({
            total: total,
            pages: Math.ceil(total/itemsPerPage),
            follows
        });
    });
}

//-------- LISTADO DE USUARIOS QUE ME SIGUEN --------//
function getFollowedUsers(req, res){
    var userId = req.user.sub;

    if(req.params.id && req.params.page){
        userId = req.params.id;
    }

    var page = 1;

    if(req.params.page){
        page = req.params.page;
    }

    var itemsPerPage = 4;

    Follow.find({followed:userId}).populate('user followed').paginate(page, itemsPerPage, (err, follows, total) => {
        if(err) return res.status(500).send({message:'Error en servidor'});

        if(!follows) return res.status(404).send({message: 'No te sigue ningún usuario'});

        return res.status(200).send({
            total: total,
            pages: Math.ceil(total/itemsPerPage),
            follows
        });
    });
}

//-------- LISTADO DE USUARIOS SIN PAGINACIÓN --------//
function getMyFollows(req,res){
    var userId = req.user.sub;
   
    var find = Follow.find({user:userId});
    
    if(req.params.followed){
        find = Follow.find({followed: userId});
    }

        find.populate('user followed').exec((err, follows) => {
            if(err) return res.status(500).send({message:'Error en servidor'});

            if(!follows) return res.status(404).send({message: 'No te sigue ningún usuario (nadie te quiere)'});

            return res.status(200).send({follows});
    });
}


module.exports = {
    saveFollow,
    deleteFollow,
    getFollowingUsers,
    getFollowedUsers,
    getMyFollows
}